include mkpm.mk
ifneq (,$(MKPM_READY))
include $(MKPM)/gnu
include $(MKPM)/envcache
include $(MKPM)/dotenv
include $(MKPM)/mkchain

REPO ?= registry.gitlab.com/bitspur/community/operator-lifecycle-manager
DOCKER ?= docker
QUILT ?= quilt
SUBMODULE ?= $$($(CD) $(PROJECT_ROOT) && $(GIT) submodule status | $(CUT) -d' ' -f3)
TAG ?= $$($(CD) $(PROJECT_ROOT)/$(SUBMODULE) && $(GIT) describe --tags --abbrev=0)

ACTIONS += submodules ##
$(ACTION)/submodules: .gitmodules
	@$(GIT) submodule update --force --init --recursive
	@$(call done,submodules)

ACTIONS += link~submodules ##
$(ACTION)/link:
	@$(LN) -s $(PROJECT_ROOT)/patches $(PROJECT_ROOT)/$(SUBMODULE)/.patches
	@$(CP) -r $(PROJECT_ROOT)/.git/modules/$(SUBMODULE) $(PROJECT_ROOT)/$(SUBMODULE)/git
	@$(call done,link)

ACTIONS += patch~link ##
$(ACTION)/patch:
	@$(QUILT) push -a || $(TRUE)
	@$(call done,patch)

ACTIONS += build~patch ##
$(ACTION)/build:
	@$(CD) $(SUBMODULE) && \
		$(DOCKER) build \
			-t "$(REPO):$(TAG)" \
			-f Dockerfile .
	@$(call done,build)

ACTIONS += push~build ##
$(ACTION)/push:
	@$(CD) operator-lifecycle-manager && \
		$(DOCKER) push "$(REPO):$(TAG)"
	@$(call done,push)

.PHONY: clean
clean: ##
	-@$(MKCACHE_CLEAN)
	-@$(JEST) --clearCache $(NOFAIL)
	-@$(CD) $(SUBMODULE) && \
		$(GIT) add -A && \
		$(GIT) reset --hard && \
		$(GIT) clean -fxd
	-@$(GIT) clean -fxd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(YARN_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

-include $(call actions)

endif
